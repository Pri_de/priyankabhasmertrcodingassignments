#include<iostream>
#include<stdlib.h>
#include "LinkedListServer.h"
#include "LinkedListInterface.h"

using namespace std;


class LinkedList : public LInterface {

	typedef struct node {
		int data;
		node *nextNode;
	}node_t;
	node_t *head; 
	node_t *tail;

	ResultCodes showLinkedList();
	ResultCodes addNewMember(int);
	ResultCodes deleteMember(int);

public: 
	LinkedList() {
		// default list member
		head = (node_t *)malloc(sizeof(node_t));
		head->data = DUMMY_NODE_DATA;
		head->nextNode = NULL;
		cout << head << "\n";
		tail = head;
	}

	~LinkedList() {
		free(head);
	}
	
	
};


LInterface* LInterface::getLinkedListObject() {
	LInterface *newList = new LinkedList();
	return newList;
}


ResultCodes LinkedList::addNewMember(int dat) {
	node_t * tempNode;

	if (head == NULL)
	{
		cout << "List is empty.";
		return S_ERROR;
	}
	else
	{
		tempNode = (node_t *)malloc(sizeof(node_t));
		tempNode->data = dat;
		tempNode->nextNode = NULL;
		tail->nextNode = tempNode;
		tail = tempNode;
		cout << dat << " - Added successfully! \n";
		return S_SUCCESS;
	}

}

ResultCodes LinkedList::showLinkedList() {
	node_t *tempNode;
	int counter = 0;

	if (head == NULL)
	{
		cout << "List is empty.\n";
		return S_ERROR;
	}
	else
	{
		tempNode = head;
		cout << "\n\n------------Linked List-------------- \n\n";

		while (tempNode != NULL) {

			if (tempNode == head) {
				cout << tempNode->data << " | " << tempNode->nextNode << " DUMMY HEAD NODE\n";
			}
			else
				cout << tempNode->data << " | " << tempNode->nextNode << "\n";
			
			tempNode = tempNode->nextNode;
			cout << "      |       \n";
			cout << "      V       \n";
			counter++;

		}
	}

	cout << "Total "<< counter << "members in the List\n\n";
	return S_SUCCESS;
}



ResultCodes LinkedList::deleteMember(int dat) {
	
	bool searchFlg = false;
	node_t *tempNode;
	node_t *prevNode;


	tempNode = head->nextNode;
	while (tempNode != NULL)
	{

		if (tempNode->data == dat)
		{

			if (tempNode == head->nextNode)
			{
				head->nextNode = tempNode->nextNode;
				free(tempNode);
				cout << dat << " - First member deleted successfully \n";
				searchFlg = true;
				return S_SUCCESS;
			}
			else if (tempNode == tail)
			{
				prevNode->nextNode = NULL;
				tail = prevNode;
				free(tempNode);
				cout << dat << " - Last member deleted successfully \n";
				searchFlg = true;
				return S_SUCCESS;
			}
			else
			{
				prevNode->nextNode = tempNode->nextNode;
				free(tempNode);
				cout << dat << " - Member deleted successfully \n";
				searchFlg = true;
				return S_SUCCESS;
			}

		}

		prevNode = tempNode;
		tempNode = tempNode->nextNode;

	}

	if (!searchFlg)
		cout << dat << " - Member not in a list\n";

	return S_ERROR;
}
