#include <iostream>
#include <conio.h>
#include "LinkedListInterface.h"


using namespace std;


int main()
{
	char test;
	LInterface *newLinkedList = LInterface::getLinkedListObject();

	if (newLinkedList->addNewMember(10) == S_SUCCESS) {
		newLinkedList->addNewMember(15);
		newLinkedList->addNewMember(45);
		newLinkedList->addNewMember(1245);
		newLinkedList->addNewMember(89);
		newLinkedList->addNewMember(445);
		newLinkedList->addNewMember(177);
		newLinkedList->addNewMember(7980);
		newLinkedList->showLinkedList();
		newLinkedList->deleteMember(74);
		newLinkedList->deleteMember(15);
		newLinkedList->deleteMember(177);
		newLinkedList->deleteMember(1245);
		newLinkedList->showLinkedList();
		getch();

	}
	else
		return 1;

}

