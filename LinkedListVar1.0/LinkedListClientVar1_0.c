#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include"LinkedListVar1_0.h"

bool gExitFlag = false;

int main (void)	
{

	int selectedOption = 0;

	printf("Linked List--- \n\n");

	while(!gExitFlag) {

		printf("\nChoose any one option and enter --> \n");
		printf("1 - Create linked list. \n");
		printf("2 - Add new member.\n");
		printf("3 - Delete member.\n");
		printf("4 - Show Linked list.\n");
		printf("5 - Exit\n");
	
		scanf("%d", &selectedOption);
		
		switch(selectedOption) {

		case 1:
		initializeLinkedList();
		break;

		case 2:
		addNewMember();
		break;

		case 3:
		deleteMember();
		break;

		case 4:
		showLinkedList();
		break;

		case 5:
		gExitFlag = true;
		printf("Exiting program ...!!\n");
		break;

		default:
		printf("select right option, try again--- \n\n");
		break;
	}

}

	return 0;
}				
			
































