#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>


typedef struct node {

	int data;
	struct node *nextNode;
	
}node_t;
	
node_t *head = NULL; 
node_t *tail = NULL;


void initializeLinkedList() {

	int dat;
	
	if(head == NULL)
	{

		head = (node_t *)malloc(sizeof(node_t));
	
		if( head == NULL) 
		{
			printf("Failed to create a node.. Please try again!\n");
			return;
		}
		else
		{
			printf("Enter data ? - \n");
			scanf("%d", &dat); 
			head->data = dat;
			head->nextNode = NULL;
			tail = head;
			printf("Head node created successfully!\n");
		}
	}
	else
	{
		printf("List already present. Please add members!\n");
	}
}


void addNewMember() {

	
	node_t * tempNode;
	int dat;
		
	if(head == NULL)
	{
		printf("List is empty. Please create the Linked list first!\n");
		return;
	}
	else
	{
		printf("Enter name of a new member - ");
		scanf("%d", &dat);
		tempNode = (node_t *)malloc(sizeof(node_t));
		tempNode->data = dat;
		tempNode->nextNode = NULL;
		tail->nextNode = tempNode;
		tail = tempNode;
		printf("Added successfully!\n");
	}

}

void showLinkedList() {

	node_t * tempNode;
	int counter = 0;

	if(head == NULL)
	{
		printf("List is empty. Please create the Linked list first!\n");
		return;
	}
	else
	{
		tempNode = head;
		printf("------------Linked List-------------- \n\n");

		while(tempNode != NULL) {

			printf("[%d|%p]\n", tempNode->data, tempNode->nextNode);
			tempNode = tempNode->nextNode;	
			printf("      |       \n");
			printf("      V       \n");
			counter++;
	
		}
	}
	
	printf("Total %d members in the List\n", counter); 
}


void deleteMember() {

	int dat;
	bool searchFlg = false;
	node_t *tempNode;
	node_t *prevNode;
	

	printf("Enter member to be deleted - \n");
	scanf("%d", &dat); 

	tempNode = head;
	while(tempNode != NULL)
	{
	
		if(tempNode->data == dat)
		{
				
			if(tempNode == head)
			{
				head = tempNode->nextNode;
				free(tempNode);
				printf("First member deleted successfully- \n");
				searchFlg = true;
				return;
			}
			else if(tempNode == tail)
			{
				prevNode->nextNode = NULL;
				tail = prevNode;
				free(tempNode);			
				printf("Last member deleted successfully- \n");
				searchFlg = true;
				return;
			}
			else
			{
				prevNode->nextNode = tempNode->nextNode;
				free(tempNode);
				printf("Member deleted successfully- \n");
				searchFlg = true;
				return;
			}
			
		}
		
		prevNode = tempNode;
		tempNode = tempNode->nextNode;
		
	}

	if(!searchFlg)
	printf("Member not in a list\n");
	

}
			
				
			
































