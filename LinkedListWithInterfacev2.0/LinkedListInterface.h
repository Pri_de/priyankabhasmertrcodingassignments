
// Linked list interface
class LInterface {

public:
	virtual ResultCodes showLinkedList(void)=0;
	virtual ResultCodes addNewMember(int)=0;
	virtual ResultCodes deleteMember(int)=0;
	
	LInterface* getLinkedListObject(void) {};

};